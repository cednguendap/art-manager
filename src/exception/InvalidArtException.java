package exception;
/**
* @author Arnaud Tako
*/

public class InvalidArtException extends java.lang.RuntimeException {

  private String entity;
  private long id;
  private String rule;
  private String message="InvalidArtException: ";

  public InvalidArtException(String entity,String rule) 
  {
      super("InvalidArtException: Entity "+entity+" must respect the rule: "+rule);
      this.message="InvalidArtException: Entity "+entity+" must respect the rule: "+rule;
      this.entity+=entity;
      this.rule=rule;
  }
  public InvalidArtException(String entity,Long id,String rule) 
  {
      super("InvalidArtException: Entity "+entity+" with id "+ id+" must respect the rule: "+rule);
      this.message="InvalidArtException: Entity "+entity+" with id "+ id+" must respect the rule: "+rule;
      this.entity+=entity;
      this.rule=rule;
      this.id=id;
  }
  public InvalidArtException(String message) 
  {
     super(message);
     this.message+=message;
  }

  
  public String toString()
    {
        return message;
    }
}