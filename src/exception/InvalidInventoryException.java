package exception;

/**
* @author Arnaud Tako
*/

public class InvalidInventoryException extends java.lang.RuntimeException {

    private String  operation;
    private String file;
    private String message="InvalidInventoryException: ";

  public InvalidInventoryException(String operation,String file) 
  {
      super("error detected during the operation"+operation+" with the file "+file);
      this.operation=operation;
      this.file=file;
      this.message+="error detected during the operation"+operation+" with the file "+file;
  }
  public InvalidInventoryException(String message)
  {
    super(message);
    this.message+=message;
  }

 public String toString()
    {
        return message;
    }
}