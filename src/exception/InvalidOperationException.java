package exception;

/**
*
* @author Arnaud Tako
*/

public class InvalidOperationException extends java.lang.RuntimeException {

    private long idArt;

    private String operation;

    private int idTransaction;

    private String message="InvalidOperationException: ";

    public InvalidOperationException(String message) {
        super(message);
        this.message+=message;
    }

    public InvalidOperationException(int idTransaction, String operation) {
        super("error detected during the operation"+operation+" with the id transaction "+idTransaction);
        this.idTransaction=idTransaction;
        this.operation=operation;
        this.message+="error detected during the operation"+operation+" with the id transaction "+idTransaction;
    }
    public InvalidOperationException(long idArt, String operation) {
        super("Error detected during the operation "+operation+" with id art "+idArt);
        this.idArt=idArt;
        this.message+="Error detected during the operation "+operation+" with id art "+idArt;
        this.operation=operation;
    }
    public String toString()
    {
        return message;
    }
}