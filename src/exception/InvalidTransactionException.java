package exception;

/**
* @author Arnaud Tako
*/

public class InvalidTransactionException extends java.lang.RuntimeException {

    private String attribut;

    private int idTransaction;
    
    private String message="InvalidTransactionException: ";

    public InvalidTransactionException(String message) {
        super(message);
        this.message+=message;
    }

    public InvalidTransactionException(int idTransaction, String attribut) 
    {
        super(idTransaction+"Unable to complete the id transaction because it is missing "+attribut);
        this.attribut=attribut;
        this.idTransaction=idTransaction;
        this.message+=idTransaction+" Unable to complete the id transaction because it is missing "+attribut;
    }

    @Override
    public String toString()
    {
       return message;
    }
}