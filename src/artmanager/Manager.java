package artmanager;

import entity.Art;
import entity.Client;
import exception.*;
import java.util.Date;
import java.util.ArrayList;
import entity.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/**
 * This class is used to manage the application
 * It serves as an interface allowing access to the different functionalities of the application
 * @author Arnaud Tako
 */
public class Manager {

    
    /**
     * List of art objects
     * 
     * @see  Inventory
     */
    private Inventory inventory;

    /**
     * List of transactions made in the application and read from the transaction file
     * @see Transaction
     * @element-type ArrayList<Transaction>
     */
    private ArrayList<Transaction> transactions=new ArrayList<>();  
    
    /**
     * URL of the file containing the list of transactions
     * @element-type String
     */
    String transactionFile="src/data/transactions.xml";
    
    /**
     * URL of the file containing the arts list
     * @element-type String
     */
    String artFile="src/data/arts.xml";
    
   
    private final DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();//For reading the XML document

    /**
     * constructor
     * @throws InvalidInventoryException when a file is missing
     * @throws RuntimeException 
     */
    public Manager() throws InvalidInventoryException,RuntimeException
    {
        read();
    }
    
    
    /**
     * This method allows to know if an art exists or not
     * @param art
     * @return true if art exists and false otherwise
     * @see Inventory#existArt(java.lang.long) 
     */
    public Boolean artExist(Art art) 
    {
        return inventory.existArt(art.getId());
    }

    /**
     * This method adds a transaction to the list of transactions
     * @param transaction 
     * @throws InvalidOperationException if the transaction already exists
     */
    public void addTransaction(Transaction transaction) throws InvalidOperationException
    {
        if(existTransaction(transaction.getId())) throw new InvalidOperationException(transaction.getId(),"adding transaction");
        transactions.add(transaction.clone());
        saveTransaction();
    }
    
    /**
     * This method verifies if a transaction exists in the list of transactions
     * @param idTransaction
     * @return true if the transaction exists in the list of transactions and false otherwise
     */
    public boolean existTransaction(int idTransaction)
    {
        for(Transaction transaction:transactions)
        {
            if(transaction.getId()==idTransaction) return true;
        }
        return false;
    }
    
    /**
     * This method removes a transaction from the transaction list
     * @param idTransaction
     * @throws InvalidOperationException if the transaction does not exist in the transaction list
     */
    public void removeTransaction(int idTransaction) throws InvalidOperationException
    {
       if(!existTransaction(idTransaction)) throw new InvalidOperationException(idTransaction,"removing transaction"); 
       int index=getIndexOfTransaction(idTransaction);
       if(transactions.get(index).isComplete()) throw new InvalidOperationException("connot remove transation. it already completed");
       transactions.remove(index);
       saveTransaction();
    }
    
    /**
     * This method returns the index of a transaction
     * @param idTransaction
     * @return the index if the transaction exists and -1 otherwise
     */
    public int getIndexOfTransaction(int idTransaction)
    {
        for(int i=0;i<transactions.size();i++)
        {
            if(transactions.get(i).getId()==idTransaction) return i;
        }
        return -1;
    }
    
    /**
     * This method completes a transaction
     * @param idTransaction
     * @throws InvalidTransactionException if the transaction was not found or if the transaction cannot be completed
     * @see Transaction#completeTransaction() 
     */
    public void completeTransaction(int idTransaction) throws InvalidTransactionException 
    {
        int index=getIndexOfTransaction(idTransaction);
        if(index==-1) throw new InvalidTransactionException(idTransaction,"not found on completing");
        if(transactions.get(index).isComplete()) throw new InvalidTransactionException(idTransaction,"is already completed");
        transactions.get(index).completeTransaction();
        for(Art art:transactions.get(index).getArts()) inventory.removeArt(art);
    }

    /**
     * This method makes it possible to find all the transactions for which the customer's email is given as a parameter.
     * @param email
     * @return the list of transactions
     */
    public ArrayList<Transaction> findTransaction(String email) {
       ArrayList<Transaction> arr=new ArrayList<>();
       for(Transaction transaction:transactions)
       {
           if(transaction.getClient()!=null && transaction.getClient().getEmail().equals(email)) arr.add(transaction.clone());
       }
       return arr;
    }

    /**
     * Allows you to search for the transaction whose id is passed in parameter
     * @param idTransaction
     * @return the transaction found and null if it does not exist
     */
    public Transaction findTransaction(int idTransaction) 
    {
        for(Transaction transaction:transactions)
       {
           if(transaction.getId()==idTransaction) return transaction.clone();
       }
       return null;
    }

    /**
     * This method allows you to search for all transactions that have a given creation date
     * @param date
     * @return the list of transactions
     */
    public ArrayList findTransaction(Date date) {

      ArrayList<Transaction> arr=new ArrayList<>();
      for(Transaction transaction:transactions)
      {
         if(transaction.getDate()==date) arr.add(transaction.clone());
      }
       return arr;
    }


    /**
     * This method allows you to add an art
     * @param art
     * @throws InvalidOperationException if the art you want to add already exists
     * @see  Inventory#addArt(entity.Art) 
     */
    public void addArt(Art art) throws InvalidOperationException
    {
        inventory.addArt(art);
    }
    
    /**
     * This method removes an art from the list of arts
     * @param idArt
     * @throws InvalidOperationException if the art to be deleted does not exist
     * @see Inventory#removeArt(entity.Art) 
     */
    public void removeArt(long idArt) throws InvalidOperationException
    {
        inventory.removeArt(idArt);
    }
    
    /**
     * This method returns the list of application arts 
     * @return the list of arts in a table
     * @see Inventory#getAllArts() 
     */
    public ArrayList<Art> getArts() {
      return inventory.getArts();
    }

    /**
     * This method allows you to add an art to a transaction
     * @param idTransaction
     * @param art 
     * @see Transaction#addArt(entity.Art) 
     */
    public void addArtToTransaction(int idTransaction, Art art) 
    {
       transactions.get(getIndexOfTransaction(idTransaction)).addArt(art);
       saveTransaction();
    }

    /**
     * This method allows to remove an art from the list of art of a transaction
     * @param idArt
     * @param idTransaction 
     * @see Transaction#removeArt(java.lang.long) 
     */
    public void removeArtFromTransaction(long idArt, int idTransaction) 
    {
        transactions.get(getIndexOfTransaction(idTransaction)).removeArt(idArt);
        saveTransaction();
    }

    /**
     * This method allows to modify the customer of an art
     * @param idTransaction
     * @param client 
     * @see Transaction#setClient(entity.Entity) 
     */
    public void changeClient(int idTransaction, Client client) 
    {
        transactions.get(getIndexOfTransaction(idTransaction)).setClient(client);
        saveTransaction();
    }
    Inventory getInventory() {
        return inventory;
    }
    private void saveTransaction() throws InvalidInventoryException
    {
        //we save art files
        String content="";
        for(Transaction transaction:transactions) content+=transaction.toString();
        write(content, "transaction");
    }
    
    public ArrayList<Transaction> findTransactionByArtId(long id)
    {
        ArrayList<Transaction> trans=new ArrayList<>();
        for(Transaction transaction:transactions)
        {
            if(transaction.existArt(id)) trans.add(transaction.clone());
        }
        return trans;
    }
    /**
     * Allows you to read the XML document passed in parameter
     * @param urlFile
     * @throws InvalidInventoryException if a file is missing
     * @throws RuntimeException If the file is not recognized
     */
    private void read(String urlFile) throws InvalidInventoryException,RuntimeException
    {
        Document document=null;
        try {               

            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            document=builder.parse(new File(urlFile));//we open the file from the builder
        }catch (Exception ex) {
            //Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
            throw new InvalidInventoryException("reading",urlFile);
        }

        Element racine=document.getDocumentElement();//we get the root element (transactions,clients or arts)
        if(racine==null) throw new RuntimeException("empty file");
        if("transactions".equals(racine.getNodeName()))
        {
            NodeList list=racine.getChildNodes();
            for(int i=0;i<list.getLength();i++)
            {            
                if(list.item(i).getNodeType()==Node.ELEMENT_NODE)
                {
                    Element node=(Element)list.item(i);
                    //on s'assure que c'est bien un noeud et pas un text simple
                    //we are in a transaction node
                    transactions.add(new Transaction(node));
                }
                
            }
        }else if("arts".equals(racine.getNodeName()) ) inventory=new Inventory(this,racine);
        else throw new RuntimeException("Unknow file");
    }
    public void read() throws InvalidInventoryException,RuntimeException
    {
        read(this.transactionFile);
        read(this.artFile);
    }
    
    
    /**
     * 
     * @param list
     * @param type
     * @throws InvalidInventoryException If the file is not present
     */
    public void write(String list,String type) throws InvalidInventoryException
    {
        String classe="art";
        if(type.equals("transaction"))
        {
            //get the name of the class that will serve as the root name for the current xml file
            classe="transaction";
        }
        String fileContent="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>"
               +"<"+classe+"s>";
       fileContent+=list;
        fileContent+="</"+classe+"s>";     
        writeToFile(fileContent,type);
    }
    
    /**
     * This method allow you to constitute the character string representing the XML tree which will be written in the xml file.
     * @param content
     * @param type
     * @throws InvalidInventoryException If the file is not present 
     */
    public void writeToFile(String content,String type) throws InvalidInventoryException
    {
        String currFile="";
        try {
            PrintWriter writer=null;
            
            if(type=="art") currFile=artFile;
            else currFile=transactionFile;
            writer=new PrintWriter(currFile);
            writer.println(content);
            writer.close();
        } catch (FileNotFoundException ex) {
            throw new InvalidInventoryException("Writting",currFile);
            //Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
}