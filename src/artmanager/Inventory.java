package artmanager;


import entity.*;
import exception.InvalidOperationException;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is used to store and manage the application arts
 * @author Arnaud Tako
 */

public class Inventory {
    

  /**
   * arts is an ArrayList which contains the list of arts
   */ 
  private ArrayList<Art> arts=new ArrayList<>();
  
  private Manager manager;
  /**
   * Constructor
   */
  public Inventory() {
  }
  public Inventory(Manager manager,Element racine)
  {
    if(manager==null) throw new IllegalArgumentException("argument manager is null");
    if(racine==null) throw new IllegalArgumentException("art file is empty");
    NodeList list=racine.getChildNodes();
    this.manager=manager;
    for(int i=0;i<list.getLength();i++)
    {            
      if(list.item(i).getNodeType()==Node.ELEMENT_NODE)
      {
        Element node=(Element)list.item(i);
        //on s'assure que c'est bien un noeud et pas un text simple
        //We collect all the art information (id, author name, price...) 
          //according to the same principle as that described above
        Art newInstance=null;
                
        if(node.getNodeName().equals("sculpture"))
        {
          newInstance=new Sculpture(node);
        }
        else if(node.getNodeName().equals("print"))
        {
          newInstance=new Print(node);
        }
        else if(node.getNodeName().equals("drawing"))
        {
          newInstance=new Drawing(node);
        }
        else if(node.getNodeName().equals("painting"))
        {
          newInstance=new Painting(node);
        }  
        checkArt(newInstance);
        arts.add(newInstance);                     
      }
    }
  }
  
  
  /**
   * 
   * allows you to add an art to the list of application arts
   * @param art
   * 
   * @throws InvalidOperationException when the art you want to add already exists
   * 
   * @see InvalidOperationException
   * 
   * @throws IllegalArgumentException if the argument art is null
   */
  public void addArt(Art art) throws InvalidOperationException,IllegalArgumentException
  {
      checkArt(art);
      arts.add(art);
      manager.write(toString(),"art");
  }

  private void checkArt(Art art) throws InvalidOperationException,IllegalArgumentException
  {
    if(art==null) throw new IllegalArgumentException("argument art is null");
    if(existArt(art.getId())) throw new InvalidOperationException(art.getId(),"add");
  }

    /**
     * This method allows to remove an art from the list of existing arts
     * 
     * @param idArt
     * 
     * @throws InvalidOperationException if the art you want to remove does not exist
     */
  public void removeArt(long idArt) throws InvalidOperationException
  {
      if(!existArt(idArt)) throw new InvalidOperationException(idArt,"remove");
      arts.remove(getIndexOf(idArt));
      manager.write(toString(),"art");
  }
  
  /**
   * Get the index of an art from its id in the arts table
   * 
   * @param idArt 
   * 
   * @return the art index whose id is known
   */
  private int getIndexOf(long idArt)
  {
      for(int i=0;i<arts.size();i++)
      {
          if(arts.get(i).getId()==idArt) return i;
      }
      return -1;
  }
  
  public Art getArt(long idArt)
  {
      Art art=null;
      int index=getIndexOf(idArt);
      if(index!=-1) art=arts.get(index).clone();
      return art;
  }
  
  /**
   * Allows you to check if an art whose id is passed in parameter exists in the table
   * 
   * @param idArt
   * 
   * @return true if the object exists in the table and false otherwise
   */
  public boolean existArt(long idArt)
  {
      for(Art art:arts)
      {
          if(art.getId()==idArt) return true;
      }
      return false;
  }        
  
  /**
   * 
   * @return 
   */
  public boolean isEmpty()
  {
     return arts.isEmpty();
  }
  
  /**
   * This method returns the list of all the arts contained in the arts property.
   * 
   * @return arts
   */
  public ArrayList<Art> getArts()
  {
    ArrayList<Art> arr=new ArrayList<>();
    for(Art art:arts) arr.add(art.clone());
      return arr;
  }
  
  /**
   * This method allows to remove an object from the list whose id is passed in parameter to avoid any 
   * rewriting of the code we call a method of the same name which takes in parameter the id of the object in parameter
   * 
   * @see removeArt(long)
   * 
   * @param art
   * 
   * @throws InvalidOperationException si l'objet a supprimer n'existe pas
   * 
   * @throws  IllegalArgumentException if the argument art is null
   *
   */
  public void removeArt(Art art) throws InvalidOperationException, IllegalArgumentException
  {
      if(art==null) throw new IllegalArgumentException("argument art is null in removeArt");
      removeArt(art.getId());
  }

  @Override
  public String toString() {
      String str="";
      for(Art art:arts)
      {
          str+=art.toString();
      }
      return str;
  }

}