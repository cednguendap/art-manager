package artmanager;

import entity.*;
import entity.Client;
import exception.InvalidOperationException;
import exception.InvalidTransactionException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Cette classe est la classe Transaction qui 
 * @author Arnaud Tako
 */
public class Transaction {

    /**
     * transaction ID
     * @element-type int
     */
    private int id;

    /**
     * total transaction price
     * @element-type Double
     */
    private Double price=0.0;

    /**
     * transaction creation date
     * @element-type Date
     */
    private Date date;
    
    /**
     * Client of the transaction
     * @element-type Client
     * @see Client
     */
    private Client client=null;

    /**
     * List of transaction arts
     * @element-type ArrayList
     * @see Art
     */
    private ArrayList<Art> arts=new ArrayList<>();

    
    /**
     * Constructor
     * @param id
     * @param arts
     * @param client 
     */
    public Transaction(int id,ArrayList<Art> arts, Client client) throws IllegalArgumentException
    {
        if(id<0 ) throw new IllegalArgumentException("argument id cannot be negative in Transaction");
        if(arts.isEmpty()) throw new IllegalArgumentException("argument art cannot be empty");
        if(client==null) throw new IllegalArgumentException("argument client cannot be null");
        if(!validArt(arts)) throw new IllegalArgumentException("art in arts list should be unique");
        this.id = id;
        this.client = client;
        this.arts=arts;
        this.date=null;
    }
    
    public Transaction(Element node)
    {
        
        //We get the date and if the date does not exist we put it on the current date
        try {
            date=node.getElementsByTagName("date").getLength()==0? (null) :new SimpleDateFormat("dd/MM/yyyy").parse(node.getElementsByTagName("date").item(0).getTextContent());
        } catch (ParseException ex) {
            Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
        }
        //we get the id
        id=Integer.valueOf(node.getAttribute("id"));
        //we access the client tag are already read the client file we just search
        // the client according to his id otherwise we read the client file first
        //if(clients.isEmpty()) read(this.clientFile);

        client=node.getElementsByTagName("client").getLength()==0?null:new Client((Element)node.getElementsByTagName("client").item(0));
        
        //we recover all the arts associated with this transaction and we search for each art according to its id

        for (int j=0;j<node.getElementsByTagName("arts").item(0).getChildNodes().getLength();j++)
        {
            // we can be sure that the current tag is of type art
            if(node.getElementsByTagName("arts").item(0).getChildNodes().item(j).getNodeType()==Node.ELEMENT_NODE)
            {
                Element artTag=(Element)node.getElementsByTagName("arts").item(0).getChildNodes().item(j);

                switch (artTag.getNodeName()) {
                    case "sculpture":
                        addArt(new Sculpture(artTag));
                        break;
                    case "print":
                        addArt(new Print(artTag));
                        break;
                    case "drawing":
                        addArt(new Drawing(artTag));
                        break;
                    case "painting":
                        addArt(new Painting(artTag));
                        break;
                    default:
                        break;
                }
            }
        }       
    }
    public boolean isComplete()
    {
        if(date==null || price==0.0 || client==null) return false;
        return true;
    }

    /**
     * This method allows to calculate the total price of the transaction respecting the given specification
     * @return le prix total calculé
     */
    public Double calculatingTotalPrice() {
       
        if(isComplete()) return price;
        else 
        {
            for(Art art:arts)
            {
                price+=art.calculateCostShipping();             
            }
            return price;
        }
    }

    @Override
    public Transaction clone()
    {
        ArrayList<Art> cloneArts=new ArrayList<>();
        for(Art art:arts)
        {
            cloneArts.add(art.clone());
        }
        return new Transaction(id,cloneArts,client.clone());
    }


    /**
     * Allows you to complete a transaction
     * @throws InvalidTransactionException if an important element is missing in the transaction
     * like art (s) or customer
     */
    public void completeTransaction() throws InvalidTransactionException
    {
        if(isComplete()) throw new InvalidTransactionException("transaction with id "+id+" already completed");
        else 
        {
            if(arts.size()==0) throw new InvalidTransactionException(id," list of arts" );
            if(client==null) throw new InvalidTransactionException(id,"client");
            date=new Date();
            calculatingTotalPrice();
        }
    }

    /**
     * This method allows you to add an art to the list of arts
     * @param art
     * @throws IllegalArgumentException if the parameter is null
     * @throws InvalidOperationException if art already exists
     */
    public void addArt(Art art) throws IllegalArgumentException,InvalidOperationException
    {
        if(isComplete()) throw new InvalidOperationException("transaction with id "+id+"are already completed");
        else 
        {
            if(art==null) throw new IllegalArgumentException("art argument is null");
            if(existArt(art.getId())) throw new InvalidOperationException(art.getId(),"on adding");
            arts.add(art.clone());
        }
    }
    
    /**
     * Allows you to return the index of the art whose id is passed in parameter
     * @param idArt
     * @return the art index if it is in the list and -1 in the constraining case
     */
    public int getIndexOf(long idArt)
    {
        for(int i=0;i<arts.size();i++)
        {
            if(arts.get(i).getId()==idArt) return i;
        }
        return -1;
    }
    
    /**
     * Allows you to check if an art whose id is passed in parameter exists
     * @param idArt
     * @return true if art is found and false otherwise
     */
    public boolean existArt(long idArt)
    {
      for(Art art:arts)
      {
          if(art.getId()==idArt) return true;
      }
      return false;
    }   

    public boolean validArt(ArrayList<Art> art)
    {
        for(int i=0;i<art.size();i++)
        {
            for(int j=i+1;j<art.size();j++)
            {
                if(art.get(i).getId()==art.get(j).getId()) return false;
            }
        }
        return true;
    }     
    
    /**
     * Removes an art from the art list
     * @param idArt 
     */
    public void removeArt(long idArt) 
    {
       int index=getIndexOf(idArt);
       if(index==-1) throw new InvalidOperationException(idArt,"on removing art transaction");
       arts.remove(index);
    }

    /**
     * Returns the list of transactional arts
     * @return list of arts
     */
    public ArrayList<Art> getArts() {
        ArrayList<Art>arr=new ArrayList<>();
        for(Art art:arts)
        {
            arr.add(art.clone());
        }
        return arr;
    }

    
    /**
     * Returns the creation date of the transaction
     * @return date
     */
    public Date getDate() {
        return (Date)date.clone();
    }

    /**
     * Returns the client of the transaction
     * @return client
     */
    public Client getClient() {
        return client.clone();
    }

    /**
     * Find the transaction price if the price is null then it recalculates
     * @return price
     */
    public Double getPrice() {
        if(isComplete()) return price;
        return calculatingTotalPrice();
    }

    /**
     * returns the transaction id
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * allows to insert the customer of the transaction
     * @param client
     * @throws IllegalArgumentException if argument is null
     */
    public void setClient(Client client) throws IllegalArgumentException
    {
        if(isComplete()) throw new InvalidOperationException("Transaction are already completed");
        if(client==null) throw new IllegalArgumentException("argument client is null");
        this.client=client.clone();
    }

    @Override
    public String toString() {        
        String content="<transaction id=\"" + id + "\" >";
        if(isComplete()) content+= "<date>" + new SimpleDateFormat("dd/MM/yyyy").format(date) + "</date>";
        content+="<price>"+price+"</price>"
                + "<arts>";        
        for(Art art:arts)
        {
            content+=art.toString();
        }
        content+="</arts>";
        
        if(client!=null) content+=client.toString();
        content+="</transaction>";
        return content;
    }
}