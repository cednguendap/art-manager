/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package artmanager;

import entity.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arnaud Tako
 */
public class ArtManager {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        scenario6();
    }
    public static void scenario1()
    {
        Manager manager=new Manager();
    }
    public static void scenario2()
    {
        Manager manager= new  Manager();
        System.out.println(manager.getInventory().toString());
    }
    public static void scenario3()
    {
        Manager manager=new Manager();
        Painting painting=new Painting(1.5,2.3, "in wood", "manual","pot of wine", 1000012100,2020, "Reeha",10.99, "House bribe", "This wooden wine pot is very pretty placed on a table");
        manager.addArt(painting);
        System.out.println(manager.getInventory().toString());
    }
    public static void scenario4()
    {
        Manager manager=new Manager();
        manager.removeArt(1000000005);
        System.out.println(manager.getInventory().toString());
        manager.removeArt(1000000009);        
    }
    public static void scenario5()
    {
        Manager manager=new Manager();
        Transaction transaction=new Transaction(2502,manager.getArts(),new Client(10002, "Tako", "Arnold","4334333", "reeha@gmail.com",new MailingAdress(123,"street","state", "zip", "city")));
        manager.addTransaction(transaction);
        System.out.println(transaction.getPrice());
        System.out.println(manager.findTransaction("reeha@gmail.com"));
    }
    public static void scenario6()
    {
        Manager manager=new Manager();
        Transaction transaction=new Transaction(500,manager.getArts(),new Client(10002, "Tako", "Arnold","4334333", "reeha@gmail.com",new MailingAdress(123,"street","state", "zip", "city")));
        manager.addTransaction(transaction);
        transaction.completeTransaction();
        System.out.println(manager.findTransaction("reeha@gmail.com"));
    }
    public static void scenario7()
    {
        Manager manager=new Manager();
        Transaction transaction=new Transaction(600,manager.getArts(),new Client(10002, "Tako", "Arnold","4334333", "reeha@gmail.com",new MailingAdress(123,"street","state", "zip", "city")));
        manager.addTransaction(transaction);
        manager.removeArtFromTransaction(1, 600);
        manager.removeArtFromTransaction(2,600);
        transaction.completeTransaction();        
    }
    public static void scenario8()
    {
        Manager manager=new Manager();
        System.out.println(manager.findTransaction("reeha@gmail.com"));
        System.out.println(manager.findTransaction(1000));
        System.out.println(manager.findTransactionByArtId(1));
        try {
            System.out.println(manager.findTransaction(new SimpleDateFormat("dd/MM/yyy").parse("20/10/2019")));
        } catch (ParseException ex) {
            Logger.getLogger(ArtManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void scenario9()
    {
        Manager manager=new Manager();
        Transaction transaction=manager.findTransaction(1000);
        transaction.completeTransaction();
        Transaction transaction2=manager.findTransaction(1000);
        System.out.println(transaction);
        System.out.println(transaction2);
    }
}
