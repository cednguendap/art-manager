package entity;

import exception.InvalidArtException;
import org.w3c.dom.Element;
/**
 * This class is an abstract class that represents the parent class of other classes
 * @author Arnaud Tako
 */

public abstract class Art {

    
    protected long id;

   
    protected int year;

    
    protected String authorName;

    protected Double price;
    
    
    protected String title;

    
    protected String description;


    /**
     * Constructor
     * @param id
     * @param year
     * @param authorName
     * @param price
     * @param title
     * @param description
     * @throws InvalidArtException if an argument is not valid
     */
    public Art(long id, int year, String authorName, Double price, String title, String description) throws InvalidArtException
    {
        this.id = id;
        this.year = year;
        this.authorName = authorName;
        this.price = price;
        this.title = title;
        this.description = description;
        checkArt();
    }
    public void checkArt() throws InvalidArtException
    {
        //On verifie que tous est OK
        if(id<1000000000) throw new InvalidArtException("Art","id should have at least 10 digits");
        if(year<=0) throw new InvalidArtException("Art","year should be >0");
        if(authorName.isEmpty()) throw new InvalidArtException("Art","authorName should not empty");
        if(price<0) throw new InvalidArtException("Art","price should be >=0");
        if(title.isEmpty()) throw new InvalidArtException("Art","title should not empty");
        if(description.isEmpty()) throw new InvalidArtException("Art","description should not empty");
        if(description.length()>500) throw new InvalidArtException("Art","description should have less than 500 characters");
        
    }
    public Art(Element node)
    {

        id=Long.valueOf(node.getAttribute("id"));
                            
        year=Integer.valueOf(node.getElementsByTagName("year").getLength()==0?"":node.getElementsByTagName("year").item(0).getTextContent());
        
        authorName=node.getElementsByTagName("authorName").getLength()==0?"":node.getElementsByTagName("authorName").item(0).getTextContent();
        
        price=Double.valueOf(node.getElementsByTagName("price").getLength()==0?"0.0":node.getElementsByTagName("price").item(0).getTextContent());
        
        title=node.getElementsByTagName("title").getLength()==0?"":node.getElementsByTagName("title").item(0).getTextContent();
        
        description=node.getElementsByTagName("description").getLength()==0?"":node.getElementsByTagName("description").item(0).getTextContent();
        checkArt();
    }
    
    public abstract Art clone();
    public long getId() {
        return id;
    }

    public int getYear() {
        return year;
    }

    public String getAuthorName() {
        return authorName;
    }

    public Double getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "<year>" + year + "</year>"
                + "<authorName>" + authorName + "</authorName>"
                + "<price>" + price + "</price>"
                + "<title>" + title + "</title>"
                + "<description>" + description  + "</description>";
    }    
    
    public Double calculateCostShipping()
    {
        return price;
    }

}