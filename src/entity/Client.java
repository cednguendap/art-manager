package entity;

import artmanager.Transaction;
import entity.MailingAdress;
import org.w3c.dom.Element;
/**
*@author Arnaud Tako
*/

public class Client {

    private int id;

    private String firstName;

    private String lastName;

    private String phone;

    private String email;

    private MailingAdress mailingAdress;

    public Client(int id, String firstName, String lastName, String phone, String email, MailingAdress mailingAdress) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.mailingAdress = mailingAdress;
    }

    public Client(Element node)
    {
        //according to the same principle as that described above
        firstName=node.getElementsByTagName("firstName").getLength()==0?"":node.getElementsByTagName("firstName").item(0).getTextContent();

        lastName=node.getElementsByTagName("lastName").getLength()==0?"":node.getElementsByTagName("lastName").item(0).getTextContent();

        phone=node.getElementsByTagName("phone").getLength()==0?"":node.getElementsByTagName("phone").item(0).getTextContent();

        email=node.getElementsByTagName("email").getLength()==0?"":node.getElementsByTagName("email").item(0).getTextContent();

        id=Integer.valueOf(node.getAttribute("id"));                

        mailingAdress=new MailingAdress(node);
    }

    public Client clone()
    {
        return new Client(id,firstName,lastName,phone,email,mailingAdress.clone());
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public MailingAdress getMailingAdress() {
        return mailingAdress;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMailingAdress(MailingAdress mailingAdress) {
        this.mailingAdress = mailingAdress;
    }

    @Override
    public String toString() {
        return "<client id=\"" + id + "\">"
                + "<firstName>" + firstName + "</firstName>"
                + "<lastName>" + lastName + "</lastName>"
                + "<phone>" + phone + "</phone>"
                + "<email>" + email + "</email>"
                +mailingAdress.toString()
                +"</client>";
    }

}