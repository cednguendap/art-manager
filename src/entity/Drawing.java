package entity;
import org.w3c.dom.Element;

/**
*@author Arnaud Tako
*/
import exception.InvalidArtException;

public class Drawing extends Art {

  private String style;

  private String technique;

  private String category;

    public Drawing(long id,String style, String technique, String category,int year, String authorName, Double price, String title, String description) throws InvalidArtException
    {
        super(id, year, authorName, price, title, description);
        if(style.isEmpty()) throw new InvalidArtException("Drawing","style should not empty");
        if(technique.isEmpty()) throw new InvalidArtException("Drawing","technique should not empty");
        if(category.isEmpty()) throw new InvalidArtException("Drawing","category should not empty");
        this.style = style;
        this.technique = technique;
        this.category = category;
    }
    public Drawing(Element node)
    {
        super(node);

        category=node.getElementsByTagName("category").getLength()==0?"":node.getElementsByTagName("category").item(0).getTextContent();
                                
        technique=node.getElementsByTagName("technique").getLength()==0?"":node.getElementsByTagName("technique").item(0).getTextContent();
        
        style=node.getElementsByTagName("style").getLength()==0?"":node.getElementsByTagName("style").item(0).getTextContent();                  
    }
    public Drawing clone()
    {
        return new Drawing(id,style,technique,category,year,authorName,price,title,description);
    }

    public String getStyle() {
        return style;
    }

    public String getTechnique() {
        return technique;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "<drawing id=\"" + id +"\">"
                +super.toString()
                +"<style>" + style + "</style>"
                + "<technique>" + technique + "</technique>"
                + "<category>" + category + "</category>"
                +"</drawing>";
    }
  
  
}