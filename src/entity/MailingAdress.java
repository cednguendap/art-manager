package entity;
import org.w3c.dom.Element;

/**
* @author Arnaud Tako
*/

public class MailingAdress {

    private int pobox;

    private String street;

    private String state;

    private String zip;

    private String city;

    public MailingAdress(int pobox, String street, String state, String zip, String city) {
        this.pobox = pobox;
        this.street = street;
        this.state = state;
        this.zip = zip;
        this.city = city;
    }

    public MailingAdress(Element node)
    {

        pobox=Integer.valueOf(node.getElementsByTagName("pobox").getLength()==0?"0":node.getElementsByTagName("pobox").item(0).getTextContent());

        street=node.getElementsByTagName("street").getLength()==0?"":node.getElementsByTagName("street").item(0).getTextContent();

        state=node.getElementsByTagName("state").getLength()==0?"":node.getElementsByTagName("state").item(0).getTextContent();

        zip=node.getElementsByTagName("zip").getLength()==0?"":node.getElementsByTagName("zip").item(0).getTextContent();

        city=node.getElementsByTagName("city").getLength()==0?"":node.getElementsByTagName("city").item(0).getTextContent();
    }

    @Override
    public MailingAdress clone()
    {
        return new MailingAdress(pobox,street,state,zip,city);
    }

    public int getPobox() {
        return pobox;
    }

    public String getStreet() {
        return street;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getCity() {
        return city;
    }

    public void setPobox(int pobox) {
        this.pobox = pobox;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "<mailingAdress>" 
                +"<pobox>" + pobox + "</pobox>"
                +"<street>" + street + "</street>"
                + "<state>" + state + "</state>"
                + "<zip>" + zip + "</zip>"
                + "<city>" + city + "</city>"
                +"</mailingAdress>";
    }  
  
}