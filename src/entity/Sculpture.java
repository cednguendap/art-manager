package entity;
import org.w3c.dom.Element;
import exception.InvalidArtException;

/**
* @author Arnaud Tako
*/


public class Sculpture extends Art {

    private String material;

    private int weight;

    public Sculpture(String material, int weight, long id, int year, String authorName, Double price, String title, String description) throws InvalidArtException
    {
        super(id, year, authorName, price, title, description);
        if(material.isEmpty()) throw new InvalidArtException("Sculpture","sculpture should not empty");
        if(weight<=0.0) throw new InvalidArtException("Sculpture","weight should be >0.0");
        this.material = material;
        this.weight = weight;
    }
    public Sculpture(Element node)
    {
        super(node);

        weight=Integer.valueOf(node.getElementsByTagName("weight").getLength()==0?"0":node.getElementsByTagName("weight").item(0).getTextContent());
                                
        material=node.getElementsByTagName("material").getLength()==0?"":node.getElementsByTagName("material").item(0).getTextContent();                            
                            
    }
    @Override
    public Sculpture clone()
    {
        return new Sculpture(material,weight,id,year,authorName,price,title,description);
    }
    public String getMaterial() {
        return material;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public Double calculateCostShipping() {
        price=10.99;
        price+=weight*0.2;
        return price;
    }

    @Override
    public String toString() {
        return "<sculpture id=\""+id+"\">"
                + super.toString()
                + "<material>" + material + "</material>"
                + "<weight>" + weight + "</weight>"
                +"</sculpture>";
    }
    
}