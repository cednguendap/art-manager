package entity;
import org.w3c.dom.Element;
import exception.InvalidArtException;

/**
*
* @author Arnaud Tako
*/

public class Print extends Art {

    private String type;

    private String category;

    public Print(long id,String type, String category,int year, String authorName, Double price, String title, String description) throws InvalidArtException
    {
        super(id, year, authorName, price, title, description);
        if(type.isEmpty()) throw new InvalidArtException("Print","type should not be empty");
        if(category.isEmpty()) throw new InvalidArtException("Print","category should not be empty");
        this.type = type;
        this.category = category;
    }
    public Print(Element node)
    {
        super(node);

        category=node.getElementsByTagName("category").getLength()==0?"":node.getElementsByTagName("category").item(0).getTextContent();
                                
        type=node.getElementsByTagName("type").getLength()==0?"":node.getElementsByTagName("type").item(0).getTextContent();                            
                            
    }
    @Override
    public Print clone()
    {
        return new Print(id,type,category,year,authorName,price,title,description);
    }
    public String getType() {
        return type;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "<print id=\"" +id+"\">"
                + super.toString()
                + "<type>" + type + "</type>"
                + "<category>" + category +"</category>"
                +"</print>";
    }
    
    
 

}