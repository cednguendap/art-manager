package entity;
import org.w3c.dom.Element;
import exception.InvalidArtException;

/**
*
* @author Arnaud Tako
*/

public class Painting extends Art {

    private Double height;

    private Double width;

    private String style;

    private String technique;

    private String category;

    public Painting(Double height, Double width, String style, String technique, String category, long id, int year, String authorName, Double price, String title, String description) {
        super(id, year, authorName, price, title, description);
        if(height<=0.0) throw new InvalidArtException("Painting","height should be >0.0");
        if(width<=0.0) throw new InvalidArtException("Painting","height should be >0.0");
        if(style.isEmpty()) throw new InvalidArtException("Painting","style should not empty");
        if(technique.isEmpty()) throw new InvalidArtException("Painting","technique should not empty");
        if(category.isEmpty()) throw new InvalidArtException("Painting","category should not empty");
        this.height = height;
        this.width = width;
        this.style = style;
        this.technique = technique;
        this.category = category;
    }

    public Painting(Element node)
    {
        super(node);      

        height=Double.valueOf(node.getElementsByTagName("height").getLength()==0?"O.O":node.getElementsByTagName("height").item(0).getTextContent());
                                
        width=Double.valueOf(node.getElementsByTagName("width").getLength()==0?"0.0":node.getElementsByTagName("width").item(0).getTextContent());
        
        style=node.getElementsByTagName("style").getLength()==0?"":node.getElementsByTagName("style").item(0).getTextContent();
        
        technique=node.getElementsByTagName("technique").getLength()==0?"":node.getElementsByTagName("technique").item(0).getTextContent();
        
        category=node.getElementsByTagName("category").getLength()==0?"":node.getElementsByTagName("category").item(0).getTextContent();                           
        
    }
    
    @Override
    public Painting clone()
    {
        return new Painting(height,width,style,technique,category,id,year,authorName,price,title,description);
    }
    @Override
    public Double calculateCostShipping() 
    {
        price=10.99;
        Double dim=width*height;     
        if(dim<100) price+=5.99;
        else if(dim>=100 && dim<=300) price+=10.99;
        else price+=15.99;
        return price;
    }

    public Double getHeight() {
        return height;
    }

    public Double getWidth() {
        return width;
    }

    public String getStyle() {
        return style;
    }

    public String getTechnique() {
        return technique;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "<painting id=\""+id+"\">"
                + super.toString()
                +"<height>" + height + "</height>"
                + "<width>" + width + "</width>"
                + "<style>" + style + "</style>"
                + "<technique>" + technique + "</technique>"
                + "<category>" + category + "</category>"
                +"</painting>";
    }
    
    

 

}